package org.test.gert.kafka.tutotial1;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoWithThreads {

	public static void main(String[] args) {
		new ConsumerDemoWithThreads().run();
	}

	private ConsumerDemoWithThreads() {}

	private void run() {
		Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThreads.class.getName());

		String bootstrapServers = "kobusa-desktop:9092";
		String groupId = "my-eight-application";
		String topic = "first_topic";

		CountDownLatch latch = new CountDownLatch(1);

		logger.info("Creating the consumer thread");
		ConsumerRunnable myConsumerRunnable = new ConsumerRunnable(
				bootstrapServers, groupId, topic, latch
		);

		Thread myThread = new Thread(myConsumerRunnable);
		myThread.start();

		// add shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread( () -> {
			logger.info("Caught shutdown hook");
			myConsumerRunnable.shutDown();
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			logger.info("Application has exited.");
		}));

		try {
			latch.await();
		} catch (InterruptedException e) {
			logger.error("Application got interrupted", e);
		} finally {
			logger.info("Application is closing");
		}
	}

	public class ConsumerRunnable implements Runnable {

		private CountDownLatch latch;
		private KafkaConsumer<String, String> consumer;

		Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class.getName());

		public ConsumerRunnable(String bootstrapServers, String groupId, String topic, CountDownLatch latch) {

			this.latch = latch;

			//create consumer configs
			Properties properties = new Properties();
			properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
			properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
			properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

			// create consumer
			consumer = new KafkaConsumer<>(properties);

			// subscribe consumer o our topic(s)
			consumer.subscribe(Arrays.asList(topic));
		}

		@Override
		public void run() {

			try {

				//poll for new data
				while (true) {
					ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
					for (ConsumerRecord<String, String> record : records) {
						logger.info("Key: " + record.key() + ", Value: " + record.value());
						logger.info("Partition : " + record.partition() + ", Offset: " + record.offset());
					}
				}
			} catch (WakeupException e) {
				logger.info("Received shutdown signal!");
			} finally {
				consumer.close();
				//tell main code we're done with countdown
				latch.countDown();
			}

		}

		public void shutDown() {

			//Will throw wakeup exception
			consumer.wakeup();

		}
	}

}
