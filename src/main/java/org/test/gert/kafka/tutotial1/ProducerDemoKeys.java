package org.test.gert.kafka.tutotial1;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.StickyAssignor;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerDemoKeys {

	public static void main(String[] args) throws ExecutionException, InterruptedException {

		final Logger logger = LoggerFactory.getLogger(ProducerDemoKeys.class);

		String bootstrapServers = "kobusa-desktop:9092";

		//create producer properties
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		//create producer
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

		for (int i = 0; i < 10; i++) {

			String topic = "first_topic";
			String value = "hello world" + i;
			String key = "id_" + i;

			//create producer record
			ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, key, value);

			logger.info("Key: " + key);

			//send data ~ asynchronous
			producer.send(record, new Callback() {
				public void onCompletion(RecordMetadata recordMetadata, Exception e) {
					//executes every time a record is successfully sent or an exception is thrown
					if (e == null) {
						//record was successfully send
						logger.info("Received new metadata: \n" +
									"Partition: " + recordMetadata.partition() + "\n" +
									"Topic: " + recordMetadata.topic() + "\n" +
									"Offset: " + recordMetadata.offset() + "\n" +
									"Timestamp: " + recordMetadata.timestamp());
					} else {
						logger.error("Error while producing", e);
					}
				}
			}).get();
		}

		//flush data
		producer.flush();
		//flush and close producer
		producer.close();

	}

}
